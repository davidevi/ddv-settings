const fs = require("fs");
const ini = require("ini");

var settings = {}

function read(settingsFilePath) {

  if (settingsFilePath.endsWith(".ini")) {
    settings = readIniSettings(settingsFilePath)
  }

}

function readIniSettings(iniFilePath) {
  return ini.parse(fs.readFileSync(iniFilePath, "utf-8"));

}

function exportSettings(targetModule = null, exportEnv = false, prefix = null) {

  if (prefix) {
    prefix += "_"
  }

  Object.entries(settings).forEach(([section_key, section_contents]) => {
    Object.entries(section_contents).forEach(([key, value]) => {

      const settingName = `${prefix}${section_key}_${key}`.toUpperCase()

      if (targetModule) {
        targetModule[settingName] = value
      }

      if (exportEnv) {
        process.env[settingName] = value;
      }

    });
  });
}


module.exports = {
  read,
  exportSettings
}
