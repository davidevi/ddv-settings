# DDV Settings

Module for easily reading and exporting settings from files

## Example

**settings.ini**
```
[a]
b = c
d = e

[f]
g = h
j = k
```

Code that loads settings from `settings.ini` and uses them:
```
const settings = require('../ddv-settings')

settings.read("settings.ini")
settings.exportSettings(this, true, "TEST")

console.log("Variable TEST_A_B == c")
console.log(this.TEST_A_B)


console.log("Variable TEST_A_B == c")
console.log(process.env["TEST_A_B"])
```

**Output:**
```
Variable TEST_A_B == c
c
Env Var TEST_F_G == h
h
```