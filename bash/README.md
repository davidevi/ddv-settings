# DDV Settings

Module for easily reading and exporting settings from files

## Example

**settings.ini**
```
[a]
b = c
d = e

[f]
g = h
j = k
```

Code that loads settings from `settings.ini` and uses them:
```#!/bin/bash

. ddv-settings "../data/settings.ini" "TF_VAR"

echo "Variable TF_VAR_A_B == c"
echo $TF_VAR_A_B

. ddv-settings "../data/settings.ini"

echo "Variable A_B == c"
echo $A_B
```

**Output:**
```
Variable TEST_A_B == c
c
Env Var TEST_F_G == h
h
```