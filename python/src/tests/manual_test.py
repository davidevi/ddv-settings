import os
import sys
from ddv.settings import read, export

read("../../../data/settings.ini")
export(sys.modules[__name__], True, "TEST")

print("Variable TEST_A_B == c")
print(TEST_A_B)

print("Env Var TEST_F_G == h")
print(os.environ.get("TEST_F_G"))